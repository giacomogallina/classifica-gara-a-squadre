extern crate find_folder;
#[macro_use]
extern crate conrod;
extern crate rumqtt;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate rand;
extern crate toml;
use conrod::backend::glium::glium;
use conrod::backend::glium::glium::Surface;
use rumqtt::{MqttOptions, MqttClient, MqttCallback, QoS};
use std::io::Read;
use std::fs::File;
use std::sync::mpsc::{Receiver, Sender, channel};
use std::sync::{Mutex, Arc};
use rand::prelude::*;
use std::str;
use std::thread;
use std::time::Duration;
use std::cmp::min;

// Generate a unique `WidgetId` for each widget.
widget_ids! {
    struct Ids {
        master,
        header,
        body,
        footer,
        //footer_scrollbar,
        matrix_box_scrollbar,

        title,
        //button_matrix,
        matrix_box,
        label_boxes[],
        labels[],
    }
}

struct TeamStatus {
    name: String,
    points: Vec<i32>,
    solved: Vec<bool>,
    total_points: i32,
    jolly: Option<usize>,
}

#[derive(Deserialize)]
struct Status {
    time_left: Option<u64>,
    teams: Vec<String>,
    problem_values: Vec<i32>,
    points: Vec<Vec<i32>>,
    solved: Vec<Vec<i32>>,
    jollys: Vec<Option<usize>>,
}

impl Status {
    fn get_teams_status(&self) -> Vec<TeamStatus> {
        let mut result: Vec<TeamStatus> = vec![];
        for i in 0..self.teams.len() {
            result.push(TeamStatus {
                name: self.teams[i].clone(),
                points: self.points[i].clone(),
                solved: self.solved[i].iter().map(|n| {
                    match *n {
                        1 => true,
                        _ => false,
                    }
                }).collect(),
                total_points: self.problem_values.len() as i32 *10 + self.points[i].iter().sum::<i32>(),
                jolly: self.jollys[i],
            });
        }
        result.sort_by_key(|t| -t.total_points);
        result
    }
}

#[derive(Deserialize, Clone)]
struct BrokerConfig {
    address: String,
    user: String,
    password: String,
}


fn main() {
    const WIDTH: u32 = 800;
    const HEIGHT: u32 = 600;

    // Build the window.
    let mut events_loop = glium::glutin::EventsLoop::new();
    let window = glium::glutin::WindowBuilder::new()
        .with_title("Canvas")
        .with_dimensions(WIDTH, HEIGHT);
    let context = glium::glutin::ContextBuilder::new()
        .with_vsync(true)
        .with_multisampling(4);
    let display = glium::Display::new(window, context, &events_loop).expect("cannot create display");

    // construct our `Ui`.
    let mut ui = conrod::UiBuilder::new([WIDTH as f64, HEIGHT as f64]).build();

    // Add a `Font` to the `Ui`'s `font::Map` from file.
    let assets = find_folder::Search::KidsThenParents(3, 5).for_folder("assets").expect("cannot find assets");
    let font_path = assets.join("fonts/NotoSans/NotoSans-Regular.ttf");
    ui.fonts.insert_from_file(font_path).expect("cannot load assets");

    // A type used for converting `conrod::render::Primitives` into `Command`s that can be used
    // for drawing to the glium `Surface`.
    let mut renderer = conrod::backend::glium::Renderer::new(&display).unwrap();

    // The image map describing each of our widget->image mappings (in our case, none).
    let image_map = conrod::image::Map::<glium::texture::Texture2d>::new();

    // Instantiate the generated list of widget identifiers.
    let ids = &mut Ids::new(ui.widget_id_generator());

    let mut status = Status {
        time_left: None,
        teams: vec![],
        points: vec![],
        solved: vec![],
        problem_values: vec![],
        jollys: vec![],
    };
    let mut teams_status: Vec<TeamStatus> = vec![];


    let mut config_file = File::open("config.toml").expect("cannot open config file");
    let mut contents = String::new();
    config_file.read_to_string(&mut contents).expect("cannot read config file");

    let config: BrokerConfig = toml::from_str(&contents).expect("cannot decode config file");

    let options = MqttOptions::new()
                    .set_keep_alive(5)
                    .set_reconnect(3)
                    .set_client_id(random::<u64>().to_string())
                    .set_broker(&config.address)
                    .set_user_name(&config.user)
                    .set_password(&config.password);

    let (tx, rx) = channel::<Status>();

    let tx = Arc::new(Mutex::new(tx));

    let callback = MqttCallback::new().on_message(move |message| {
        //println!("hey, i got a message!:\n{}", str::from_utf8(&*message.payload).unwrap());
        let tx = tx.lock().unwrap();
        match serde_json::from_slice(&*message.payload) {
            Ok(info) => tx.send(info).unwrap(),
            Err(_) => (),
        }
    });

    let mut client = MqttClient::start(options, Some(callback)).expect("cannot connect to mqtt broker");
    client.subscribe(vec![("status", QoS::Level2)]).expect("cannot subscribe");

    // Poll events from the window.
    let mut event_loop = EventLoop::new();
    'main: loop {

        // Handle all events.
        for event in event_loop.next(&mut events_loop) {

            // Use the `winit` backend feature to convert the winit event to a conrod one.
            if let Some(event) = conrod::backend::winit::convert_event(event.clone(), &display) {
                ui.handle_event(event);
                event_loop.needs_update();
            }

            match event {
                glium::glutin::Event::WindowEvent { event, .. } => match event {
                    // Break from the loop upon `Escape`.
                    glium::glutin::WindowEvent::Closed |
                    glium::glutin::WindowEvent::KeyboardInput {
                        input: glium::glutin::KeyboardInput {
                            virtual_keycode: Some(glium::glutin::VirtualKeyCode::Escape),
                            ..
                        },
                        ..
                    } => break 'main,
                    _ => (),
                },
                _ => (),
            }
        }

        for s in rx.try_iter() {
            status = s;
            //println!("new status!");
            teams_status = status.get_teams_status();
        }

        // Instantiate all widgets in the GUI.
        set_widgets(ui.set_widgets(), ids, &mut status, &mut teams_status);

        // Render the `Ui` and then display it on the screen.
        if let Some(primitives) = ui.draw_if_changed() {
            renderer.fill(&display, primitives, &image_map);
            let mut target = display.draw();
            target.clear_color(0.0, 0.0, 0.0, 1.0);
            renderer.draw(&display, &mut target, &image_map).unwrap();
            target.finish().unwrap();
        }
    }
}

// Draw the Ui.
fn set_widgets(ref mut ui: conrod::UiCell, ids: &mut Ids, status: &mut Status, teams_status: &mut Vec<TeamStatus>) {
    use conrod::{color, widget, Colorable, Borderable, Labelable, Positionable, Sizeable, Widget};

    let cols = status.problem_values.len() + 2;
    let rows = teams_status.len() + 2;

    ids.label_boxes.resize((cols+1)*rows, &mut ui.widget_id_generator());
    ids.labels.resize(cols*rows, &mut ui.widget_id_generator());


    // Construct our main `Canvas` tree.
    widget::Canvas::new().flow_down(&[
        (ids.header, widget::Canvas::new().color(color::rgb(0.1, 0.1, 0.1)).pad_bottom(20.0).length(100.0)),
        (ids.footer, widget::Canvas::new().color(color::WHITE)),
    ]).set(ids.master, ui);

    // A scrollbar for the `FOOTER` canvas.
    widget::Scrollbar::y_axis(ids.matrix_box).auto_hide(true).set(ids.matrix_box_scrollbar, ui);


    widget::Text::new("Classifica")
        .color(color::LIGHT_ORANGE)
        .font_size(48)
        .middle_of(ids.header)
        .set(ids.title, ui);

    let mut footer_wh = ui.wh_of(ids.footer).unwrap();
    if footer_wh[0] > (60*(cols+5)) as f64  && footer_wh[1] > (60*rows) as f64 {
        footer_wh[0] = (60*(cols+5)) as f64;
        footer_wh[1] = (60*rows) as f64;
    }
    else if footer_wh[0] > footer_wh[1]/(rows as f64)*((cols+5) as f64) {
        footer_wh[0] = footer_wh[1]/(rows as f64)*((cols+5) as f64);
    }
    else if footer_wh[1] > footer_wh[0]/((cols+5) as f64)*(rows as f64) {
        footer_wh[1] = footer_wh[0]/((cols+5) as f64)*(rows as f64);
    }

    let mut lines = vec![vec![]; cols];
    let mut matrix = vec![];

    for r in 0..rows {
        for c in 0..cols {
            let mut new_canvas = widget::Canvas::new().color(color::WHITE);
            if c > 1 && r > 1 {
                if teams_status[r-2].solved[c-2] {
                    new_canvas = new_canvas.color(color::rgb(0.9, 1.0, 0.9));
                }
                else if teams_status[r-2].points[c-2] < 0 {
                    new_canvas = new_canvas.color(color::rgb(1.0, 0.9, 0.9));
                }
                if let Some(j) = teams_status[r-2].jolly {
                    if j == c-2 {
                        new_canvas = new_canvas.border(5.0);
                    }
                }
            }
            else {
                new_canvas = new_canvas.border(0.0);
            }
            if c == 0 {
                new_canvas = new_canvas.length_weight(5.0);
            }
            else if c == 1 {
                new_canvas = new_canvas.length_weight(2.0);
            }
                
            lines[r].push((ids.label_boxes[r*(cols+1)+c+1], new_canvas));
        }
    }

    for r in 0..rows {
        matrix.push((ids.label_boxes[r*(cols+1)], widget::Canvas::new().flow_right(&lines[r])));
    }

    let matrix_box = widget::Canvas::new()
        .color(color::WHITE)
        .flow_down(&matrix)
        .scroll_kids_vertically()
        .mid_top_with_margin_on(ids.footer, 2.0)
        .w_h(footer_wh[0], footer_wh[1])
        .set(ids.matrix_box, ui);

    for r in 0..rows {
        for c in 0..cols {
            let label_text = match r {
                0 => {
                    match c {
                        0 => {
                            match status.time_left {
                                Some(tl) => format!("{} : {}", tl / 60, tl % 60),
                                None => "Not Started".to_string(),
                            }
                        },
                        1 => String::new(),
                        _ => (c-1).to_string(),
                    }
                },
                1 => match c {
                    0 | 1 => String::new(),
                    _ => status.problem_values[c-2].to_string(),
                }
                _ => {
                    match c {
                        0 => teams_status[r-2].name.clone(),
                        1 => teams_status[r-2].total_points.to_string(),
                        _ => teams_status[r-2].points[c-2].to_string(),
                    }
                }
            };
            let mut label = widget::Text::new(&label_text).font_size(24).color(color::BLACK).center_justify();
            if c > 1 && r > 1 {
                if teams_status[r-2].solved[c-2] {
                    label = label.color(color::DARK_GREEN);
                }
                else if teams_status[r-2].points[c-2] < 0 {
                    label = label.color(color::DARK_RED);
                }
            }
            label.middle_of(ids.label_boxes[r*(cols+1)+c+1]).set(ids.labels[r*cols+c], ui);
        }
    }

    //let mut elements = widget::Matrix::new(cols, rows)
        //.w_h(footer_wh[0], min(footer_wh[1] as usize, 100*rows) as f64)
        //.mid_top_of(ids.footer)
        //.set(ids.button_matrix, ui);
    //while let Some(elem) = elements.next(ui) {
        //let (r, c) = (elem.row, elem.col);
        ////println!("{}, {}", r, c);
        //let label_text = match r {
            //0 => {
                //match c {
                    //0 => {
                        //match status.time_left {
                            //Some(tl) => format!("{} : {}", tl / 60, tl % 60),
                            //None => "Not Started".to_string(),
                        //}
                    //},
                    //1 => String::new(),
                    //_ => status.problem_values[c-2].to_string(),
                //}
            //},
            //_ => {
                //match c {
                    //0 => teams_status[r-1].name.clone(),
                    //1 => teams_status[r-1].total_points.to_string(),
                    //_ => teams_status[r-1].points[c-2].to_string(),
                //}
            //}
        //};
        //let mut label_box = widget::Canvas::new().color(color::WHITE).border(0.9);
        //let mut label = widget::Text::new(&label_text).font_size(20).color(color::BLACK).center_justify();
        //if c > 1 && r > 0 {
            //if teams_status[r-1].solved[c-2] {
                //label = label.color(color::DARK_GREEN);
                //label_box = label_box.color(color::rgb(0.9, 1.0, 0.9));
            //}
            //else if teams_status[r-1].points[c-2] < 0 {
                //label = label.color(color::DARK_RED);
                //label_box = label_box.color(color::rgb(1.0, 0.9, 0.9));
            //}
            ////label = label.font_size(24);
            //if let Some(j) = teams_status[r-1].jolly {
                //if j == c-2 {
                    //label_box = label_box.border(5.0);
                //}
            //}
        //}
        //else {
            //label_box = label_box.border(0.0);
        //}
        ////label_box.set(ids.label_boxes[cols*r + c], ui);
        //label = label.middle_of(elem.widget_id);
               
        ////let label = widget::Canvas::new().label(&label_text).label_color(color::BLACK).color(color::WHITE);
        ////if c == 0 {
            ////elem.w = 500.0;
        ////}

        //elem.set(label_box, ui);
        //label.set(ids.label_boxes[cols*r+c], ui);
        ////elem.set(label, ui);
    //}
}


pub struct EventLoop {
    ui_needs_update: bool,
    last_update: std::time::Instant,
}

impl EventLoop {
    pub fn new() -> Self {
        EventLoop {
            last_update: std::time::Instant::now(),
            ui_needs_update: true,
        }
    }

    /// Produce an iterator yielding all available events.
    pub fn next(
        &mut self,
        events_loop: &mut glium::glutin::EventsLoop,
    ) -> Vec<glium::glutin::Event> {

        // We don't want to loop any faster than 60 FPS, so wait until it has been at least 16ms
        // since the last yield.
        //let last_update = self.last_update;
        //let sixteen_ms = std::time::Duration::from_millis(16);
        //let duration_since_last_update = std::time::Instant::now().duration_since(last_update);
        //if duration_since_last_update < sixteen_ms {
            //std::thread::sleep(sixteen_ms - duration_since_last_update);
        //}
        thread::sleep(Duration::from_millis(100));

        // Collect all pending events.
        let mut events = Vec::new();
        events_loop.poll_events(|event| events.push(event));

        // If there are no events and the `Ui` does not need updating, wait for the next event.
        //if events.is_empty() && !self.ui_needs_update {
            //events_loop.run_forever(|event| {
                //events.push(event);
                //glium::glutin::ControlFlow::Break
            //});
        //}

        self.ui_needs_update = false;
        self.last_update = std::time::Instant::now();

        events
    }

    /// Notifies the event loop that the `Ui` requires another update whether or not there are any
    /// pending events.
    ///
    /// This is primarily used on the occasion that some part of the `Ui` is still animating and
    /// requires further updates to do so.
    pub fn needs_update(&mut self) {
        self.ui_needs_update = true;
    }
}


